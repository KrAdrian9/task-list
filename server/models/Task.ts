import { Schema, model } from "mongoose";

const TaskSchema = new Schema({
  description: {
    type: String,
    required: true,
  },
  isDone: {
    type: Boolean,
    required: true,
  },
});

const TasksList = model("taskLists", TaskSchema);

export default TasksList;
