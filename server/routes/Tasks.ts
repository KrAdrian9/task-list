import { Router } from "express";

import {
  getAllTask,
  deleteTask,
  createTask,
  updateStatus,
} from "../controllers/Task";

const router = Router();

router.get("/", getAllTask);
router.post("/", createTask);
router.delete("/:id", deleteTask);
router.put("/:id", updateStatus);

export default router;
