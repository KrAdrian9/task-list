import {Request, Response} from "express";
import Task from "../models/Task";

export const getAllTask = async (req: Request, res: Response) => {
    try {
        const tasks = await Task.find();

        if (!tasks) {
            res.status(404).json({msg: "task not found"})
        }

        res.status(200).json(tasks);

    } catch (e: any) {
        res.status(500).json({msg: e.message});
    }
};

export const updateStatus = async (req: Request, res: Response) => {
    try {
        const {
            body,
            params: {id},
        } = req;

        const task = await Task.findByIdAndUpdate({_id: id}, body, {
            new: true,
        });

        if (!task) {
            res.status(404).json({msg: "task not found"})
        }

        res.status(200).json(task);
    } catch (e: any) {
        res.status(500).json({msg: e.message, success: false});
    }
};

export const createTask = async (req: Request, res: Response) => {
    try {
        const newTask = new Task(req.body);

        const task = await newTask.save();
        if (!task) {
            res.status(404).json({msg: "fail in create new task"})
        }
        res.status(200).json(task);
    } catch (e: any) {
        res.status(500).json({msg: e.message, success: false});
    }
};

export const deleteTask = async (req: Request, res: Response) => {
    try {
        const task = await Task.findByIdAndDelete(req.params.id);

        if (!task) {
            res.status(404).json({msg: "task not found"})
        }
        res.status(200).json({success: true});
    } catch (e: any) {
        res.status(500).json({msg: e.message, success: false});
    }
};
