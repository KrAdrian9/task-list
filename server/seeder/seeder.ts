import faker from "faker";
import Task from "../models/Task";

export const seedTasks = () => {
  try {
    for (let i = 0; i < 2000; i++) {
      new Task({
        description: faker.lorem.sentence(10),
        isDone: faker.datatype.boolean(),
      })
        .save()
        .then();
    }
  } catch (error) {
    console.log("error");
  }
};
