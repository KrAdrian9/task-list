import express from "express";
import bodyParser from "body-parser";
import mongoose from "mongoose";

import taskRouter from "./routes/Tasks";

const app = express();
app.use(bodyParser.json());

mongoose
  .connect(
    "mongodb+srv://test:test123@cluster0.koo0m.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
  )
  .then(() => console.log("db connected"))
  .catch(() => console.log("error"));

app.use("/api/tasks", taskRouter);

app.listen(8000, () => {
  console.log(`server run on port `);
});
