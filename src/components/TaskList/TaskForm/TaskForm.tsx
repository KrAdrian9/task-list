import React, { FormEvent } from "react";

import {
  Button,
  DialogActions,
  DialogTitle,
  TextField,
  DialogContent,
  FormControl,
  Dialog,
} from "@material-ui/core";

import { useForm } from "./Hook/UseForm";

import { TaskPayload } from "../../../types/Task";

interface Iprops {
  isVisible: boolean;
  closeModal: () => void;
  onSaveTask: (payload: TaskPayload) => void;
}

const TaskForm = ({ isVisible, closeModal, onSaveTask }: Iprops) => {
  const { onChangeInputValue, formIsValid, errors, description } = useForm();

  const handleFormSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (formIsValid()) {
      const payload = {
        description,
        isDone: false,
      };

      onSaveTask(payload);
    }
  };

  return (
    <Dialog
      fullWidth={true}
      open={isVisible}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">Dodaj Zadanie</DialogTitle>
      <DialogContent>
        <form onSubmit={handleFormSubmit} autoComplete="off">
          <FormControl fullWidth={true} required>
            <TextField
              required={true}
              autoFocus
              label="Opis zadania"
              name="description"
              fullWidth
              onChange={onChangeInputValue}
              {...(errors && {
                error: true,
                helperText: "Opis zadania jest wymagany",
              })}
            />
          </FormControl>
          <DialogActions>
            <Button onClick={closeModal} color="primary">
              Anuluj
            </Button>
            <Button type="submit" color="primary" disabled={!formIsValid()}>
              Zapisz
            </Button>
          </DialogActions>
        </form>
      </DialogContent>
    </Dialog>
  );
};

export default TaskForm;
