import React from "react";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";

import TaskForm from "../TaskForm";

const saveTask = jest.fn();
const closeModal = jest.fn();

jest.mock("../Hook/UseForm", () => ({
  useForm: jest.fn(() => ({
    description: "task description",
    formIsValid: jest.fn(() => true),
  })),
}));

const renderTaskForm = () => {
  render(
    <TaskForm isVisible={true} onSaveTask={saveTask} closeModal={closeModal} />
  );
};

describe("Task list modal", () => {
  it("should display with correct inputs and button", async () => {
    renderTaskForm();

    expect(screen.getByText(/Dodaj Zadanie/i)).toBeInTheDocument();
    expect(screen.getByRole("button", { name: /Zapisz/i })).toBeInTheDocument();
    expect(screen.getByRole("button", { name: /Anuluj/i })).toBeInTheDocument();
  });

  it("should call save function after click button", async () => {
    renderTaskForm();

    const saveButton = screen.getByRole("button", { name: /Zapisz/i });

    fireEvent.click(saveButton);

    await waitFor(() => {
      expect(saveTask).toBeCalledTimes(1);
      expect(saveTask).toBeCalledWith(
        expect.objectContaining({
          description: "task description",
          isDone: false,
        })
      );
    });
  });

  it("should call close modal function after click cancel button", async () => {
    renderTaskForm();

    const cancelButton = screen.getByRole("button", { name: /Anuluj/i });

    fireEvent.click(cancelButton);

    await waitFor(() => {
      expect(closeModal).toBeCalledTimes(1);
    });
  });
});
