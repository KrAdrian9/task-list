import { useState } from "react";

export const useForm = () => {
  const [description, setDescription] = useState("");
  const [errors, setError] = useState("");

  const validate: any = (value: string) => {
    value.length === 0 ? setError("Opisa Zadania jest wymagany") : setError("");
  };

  const onChangeInputValue = (e: any) => {
    const { value } = e.target;

    setDescription(value);
    validate(value);
  };

  const formIsValid = (): boolean =>
    errors?.length === 0 && description?.length !== 0;

  return {
    onChangeInputValue,
    formIsValid,
    errors,
    description,
  };
};
