import React, { useEffect, useState, ChangeEvent } from "react";

import * as Styled from "./TaskList.styles";
import DeleteIcon from "@material-ui/icons/Delete";
import SearchIcon from "@material-ui/icons/Search";

import {
  Checkbox,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  IconButton,
  Button,
  CircularProgress,
} from "@material-ui/core";
import Pagination from "@material-ui/lab/Pagination";

import { Task, TaskPayload } from "../../types/Task";
import TaskForm from "./TaskForm/TaskForm";

import {
  createTask,
  deleteTask,
  getTaskList,
  updateStatus,
} from "../../services/Tasks";

const TaskList = () => {
  const [tasks, setTasks] = useState<Task[]>([]);
  const [searchTasks, setSearchTasks] = useState<Task[]>([]);
  const [tasksPerPage, setTasksPerPage] = useState<Task[]>([]);
  const [isVisible, setVisible] = useState(false);
  const [page, setPage] = useState(1);
  const [search, setSearch] = useState("");

  useEffect(() => {
    getTaskList().then((response) => {
      const sortedTasks = sortTasksByStatus(response);

      setTasks(sortedTasks);
      setSearchTasks(sortedTasks);
    });
  }, []);

  useEffect(() => {
    if (search) sortTaskBySearchValue(false);
    else {
      const taskList = sortTasksByStatus(tasks);
      setSearchTasks(taskList);
    }
  }, [tasks]);

  useEffect(() => {
    if (!search) sortTaskBySearchValue(true);
  }, [search]);

  useEffect(() => {
    const tasks = searchTasks.slice((page - 1) * 50, page * 50 - 1);
    setTasksPerPage(tasks);
  }, [page, searchTasks]);

  const sortTaskBySearchValue = (scroll: boolean) => {
    const sortedTasks = sortTasksByStatus(tasks).filter(({ description }) =>
      description.toLowerCase().includes(search.toLowerCase())
    );
    setSearchTasks(sortedTasks);
    if (scroll) setPage(1);
  };

  const saveTask = (payload: TaskPayload) => {
    createTask(payload).then((response) => {
      setTasks([response, ...tasks]);
      closeModal();
    });
  };

  const onDeleteTask = (id: string) => {
    deleteTask(id).then(() => {
      const newTaskList = tasks.filter(({ _id }) => _id !== id);
      setTasks(newTaskList);
      consoleLogDoneTasks(newTaskList)
    });
  };

  const changeStatus = (id: string) => {
    const index = tasks.findIndex(({ _id }) => _id === id);

    if (index !== -1) {
      const { isDone } = tasks[index];
      const payload = {
        isDone: !isDone,
      };

      updateStatus(payload, id).then((response) => {
        tasks.splice(index, 1, response);

        setTasks([...tasks]);
        consoleLogDoneTasks(tasks);
      });
    }
  };

  const consoleLogDoneTasks = (tasks: Task[]) => {
    const filterTasks = tasks.filter(({ isDone }) => isDone);
    console.log(filterTasks);
  };

  const onChangeSearch = (e: ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    setSearch(value);
  };

  const sortTasksByStatus = (taskList: Task[]) =>
    taskList.sort((a, b) => (a.isDone === b.isDone ? 0 : a.isDone ? 1 : -1));

  const closeModal = () => {
    setVisible(false);
  };

  return (
    <Styled.Container>
      <Styled.ContentContainer>
        <Styled.ActionsContainer>
          <Styled.SearchContainer>
            <Styled.InputSearch
              fullWidth
              placeholder="Wyszukaj"
              onChange={onChangeSearch}
            />
            <IconButton
              type="submit"
              aria-label="search"
              onClick={() => sortTaskBySearchValue(true)}
            >
              <SearchIcon />
            </IconButton>
          </Styled.SearchContainer>
          <Button
            variant="outlined"
            color="primary"
            onClick={() => setVisible(true)}
          >
            Dodaj zadanie
          </Button>
        </Styled.ActionsContainer>
        {tasks && tasks.length === 0 ? (
          <Styled.CircularProgressContainer>
            <CircularProgress />
          </Styled.CircularProgressContainer>
        ) : (
          <Styled.TaskListContainer>
            <Styled.TaskList>
              {tasksPerPage.map(({ _id, description, isDone }) => (
                <ListItem button>
                  <Checkbox
                    checked={isDone}
                    onChange={() => changeStatus(_id)}
                    edge="start"
                    disableRipple
                  />
                  <ListItemText primary={description} />
                  <ListItemSecondaryAction>
                    <IconButton
                      onClick={() => onDeleteTask(_id)}
                      edge="end"
                      aria-label="delete"
                      color="secondary"
                    >
                      <DeleteIcon />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
              ))}
            </Styled.TaskList>
          </Styled.TaskListContainer>
        )}
        <Styled.PaginationContainer>
          <Pagination
            page={page}
            variant="outlined"
            shape="rounded"
            count={Math.round(searchTasks.length / 50)}
            color="primary"
            onChange={(e, value) => setPage(value)}
          />
        </Styled.PaginationContainer>
        <TaskForm
          isVisible={isVisible}
          closeModal={closeModal}
          onSaveTask={saveTask}
        />
      </Styled.ContentContainer>
    </Styled.Container>
  );
};

export default TaskList;
