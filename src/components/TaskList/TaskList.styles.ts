import styled from "styled-components";

import { List, Paper, InputBase } from "@material-ui/core";

export const Container = styled.div`
  display: flex;
  justify-content: center;
  width: 100vw;
  height: 100vh;
`;

export const ContentContainer = styled.div`
  display: flex;
  width: 50%;
  flex-direction: column;
`;

export const ActionsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  margin: 10px 0;
`;

export const SearchContainer = styled(Paper)`
  display: flex;
  height: 70%;
  width: 75%;
  margin-right: 20px;
`;

export const InputSearch = styled(InputBase)`
  margin: 0 10px;
`;

export const TaskListContainer = styled.div`
  overflow: auto;
  width: 100%;
  display: flex;
`;

export const TaskList = styled(List)`
  width: 100%;
`;

export const CircularProgressContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
`;

export const PaginationContainer = styled.div`
  height: auto;
  margin: 20px 0;
  display: flex;
  justify-content: end;
`;
