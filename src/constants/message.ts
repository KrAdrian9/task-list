export const MESSAGE = {
  CREATE: "Pomyślnie dodano zadanie",
  DELETE: "Pomyślnie usunięto zadanie",
  ERROR: "Wystapił nieznany błąd, spróbuj ponownie później",
  UPDATE: "Pomyślnie zaktualizowanostatus zadania",
};
