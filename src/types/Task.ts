import { Omit } from "@material-ui/core";

export interface Task {
  _id: string;
  description: string;
  isDone: boolean;
}

export type TaskPayload = Omit<Task, "_id">;
