import axios from "axios";
import { Task, TaskPayload } from "../types/Task";

export const getTaskList = (): Promise<Task[]> => {
  return new Promise((resolve, reject) => {
    const url = "/api/tasks";

    return axios(url, {
      method: "GET",
      headers: {
        "content-type": "application/json",
      },
    })
      .then(({ data }) => resolve(data))
      .catch((error) => reject(error));
  });
};

export const createTask = (payload: TaskPayload): Promise<Task> => {
  return new Promise((resolve, reject) => {
    const url = "api/tasks";

    return axios(url, {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },
      data: JSON.stringify(payload),
    })
      .then(({ data }) => resolve(data))
      .catch((error) => reject(error));
  });
};

export const deleteTask = (id: string): Promise<void> => {
  return new Promise((resolve, reject) => {
    const url = `api/tasks/${id}`;

    return axios(url, {
      method: "DELETE",
      headers: { "content-type": "applicaiton/json" },
    })
      .then(() => resolve())
      .catch((error) => reject(error));
  });
};

type Payload = {
  isDone: boolean;
};

export const updateStatus = (payload: Payload, id: string): Promise<Task> => {
  return new Promise((resolve, reject) => {
    const url = `api/tasks/${id}`;

    return axios(url, {
      method: "PUT",
      headers: { "content-type": "application/json" },
      data: JSON.stringify(payload),
    })
      .then(({ data }) => resolve(data))
      .catch((error) => reject(error));
  });
};
