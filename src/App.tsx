import React from "react";

import "./index.css";
import TaskList from "./components/TaskList/TaskList";

function App() {
  return <TaskList />;
}

export default App;
